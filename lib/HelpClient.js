const EventEmitter = require("events");
const log = require("log4js").getLogger("help-client");
const { Socket } = require("net");
const readline = require("readline");

const HEARTBEAT_INTERVAL = 2000;
const EVENT_RESPONSE = "response";
const EVENT_CONNECT = "connect";
const EVENT_DISCONNECT = "disconnect";

const EVENT_CONNECT_ERROR = "connect-error";
const EVENT_RESPONSE_ERROR = "response-error";

/**
 * Class for interacting with the test server
 */
class HelpClient extends EventEmitter {
  /**
   * Create instance of client
   *
   * @param name The user name
   * @param host The host to connect to
   * @param port The port to connect to
   */
  constructor(name, host, port) {
    super();
    log.debug(
      `constructor(${JSON.stringify(name)}, ${JSON.stringify(host)}, ${port})`
    );

    this.name = name;
    this.host = host;
    this.port = port;
    this.nextId = 1;
    this.heartBeatInterval = HEARTBEAT_INTERVAL;

    // Events raised when a line arrives from server or when connection is dropped
    this.addListener(EVENT_RESPONSE, response => {
      log.debug(`events.on ${EVENT_RESPONSE}:`, response);
      if (response.type === "heartbeat") {
        this.restartHeartbeatMonitor();
      }
    });

    /**
     * Closes socket, if currently open
     * @returns {Promise<void>}
     */
    this.closeSocket = async function closeSocket() {
      const socket = this.socket;
      if (socket) {
        socket.end();
        socket.destroy();
      }
    };

    /**
     * (Re)starts monitor to wait for next heartbeat
     */
    this.restartHeartbeatMonitor = function restartMonitor() {
      log.debug("restartHeartbeatMonitor");
      this.cancelHeartbeatMonitor();
      this.monitorTimeout = setTimeout(async () => {
        try {
          await this.open();
        } catch (error) {
          log.error(
            "restartHeartbeatMonitor: Cannot reconnect to server",
            error
          );
        }
      }, this.heartBeatInterval);
      log.trace("heartbeatMonitor started");
    };

    /**
     * Stops monitoring heartbeat
     */
    this.cancelHeartbeatMonitor = function endMonitor() {
      if (this.monitorTimeout) {
        log.trace("heartbeatMonitor canceled");
        clearTimeout(this.monitorTimeout);
      }
    };

    /**
     * Sends payload to server and returns the value of the response msg.
     *
     * @param {Object} payload Object to send to the server
     * @returns {Promise<*>} response.msg returned from server
     */
    this.remoteCall = async function remoteCall(payload) {
      // Add a unique id to the request to properly correlate response
      const id = String(`call#${this.nextId++}`);
      let onResonse;
      let onDisconnected;

      return new Promise((resolve, reject) => {
        this.on(
          EVENT_RESPONSE,
          (onResonse = response => {
            if (response.type === "msg" && response.msg.reply === id) {
              log.info("remoteCall response:", response);
              // Return result without the id
              const { reply: _reply, ...result } = response.msg;
              resolve(result);
            }
          })
        );
        this.on(
          EVENT_DISCONNECT,
          (onDisconnected = () => {
            log.warn(
              "request failed: socket disconnected",
              `[${JSON.stringify(payload)}]`
            );
            reject(new Error("Socket disconnected"));
          })
        );
        this.sendRequest({ ...payload, id }).catch(reject);
      }).finally(() => {
        // Remove event handlers to avoid memory leaks
        this.removeListener(EVENT_RESPONSE, onResonse);
        this.removeListener(EVENT_DISCONNECT, onDisconnected);
      });
    };
  }

  /**
   * Open connection to server
   *
   * @returns {Promise<void>}
   */
  async open() {
    log.debug("open()");

    this.closeSocket();

    const socket = (this.socket = new Socket());

    // Add connection listeners
    socket.on("close", () => {
      log.debug("socket closed");
      this.emit(EVENT_DISCONNECT);
      if (this.socket === socket) {
        this.socket = undefined;
      }
    });

    // Add line read listener
    readline.createInterface({ input: socket }).on("line", line => {
      log.debug("socket received", line);
      try {
        const response = JSON.parse(line);
        this.emit(EVENT_RESPONSE, response);
      } catch (error) {
        log.warn("invalid server response:", error.message, `[${line}]`);
        this.emit(EVENT_RESPONSE_ERROR, {
          error,
          line
        });
      }
    });

    await new Promise((resolve, reject) => {
      socket.on("connect", () => {
        log.debug("socket connected");
        this.emit(EVENT_CONNECT, socket);
        this.restartHeartbeatMonitor();
        resolve();
      });
      socket.on("error", err => {
        log.error("failed to connect socket", err);
        this.emit(EVENT_CONNECT_ERROR, err);
        reject(err);
      });

      socket.connect({
        port: this.port,
        host: this.host
      });
    });

    await this.sendRequest({ name: this.name });
  }

  /**
   * Request the current count
   *
   * @returns {Promise<{ count: number }>} Current count response
   */
  async getCount() {
    return this.remoteCall({ request: "count" });
  }

  /**
   * Request the current time
   *
   * @returns {Promise<{ time: string, random: number }>} Current count response
   */
  async getTime() {
    return this.remoteCall({ request: "time" });
  }

  /**
   * Send a message to server
   *
   * @param {object} payload The payload to send
   * @returns {Promise<void>} Resolves when message sent
   */
  async sendRequest(payload) {
    log.info("sendRequest:", payload);
    return new Promise((resolve, reject) => {
      if (!this.socket) {
        reject(new Error("Not connected to server"));
      } else {
        this.socket.write(`${JSON.stringify(payload)}\r\n`, err => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      }
    });
  }

  /**
   * Close connection to server
   *
   * @returns {Promise<void>}
   */
  async close() {
    this.cancelHeartbeatMonitor();
    await this.closeSocket();
  }
}

module.exports = {
  EVENT_RESPONSE,
  EVENT_CONNECT,
  EVENT_DISCONNECT,
  EVENT_RESPONSE_ERROR,
  HelpClient
};
