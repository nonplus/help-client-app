const delay = require("delay");
const net = require("net");
const doListen = require("do-listen");
const readline = require("readline");

const { HelpClient } = require("./HelpClient");

const name = "name";
const welcome = { msg: "Welcome ~~ ${name}!", type: "welcome" };
const heartbeat = { type: "heartbeat", epoch: 1561765173 };
const response = { msg: "Some Message", type: "response" };

const welcomeLine = `${JSON.stringify(welcome)}\r\n`;
const heartbeatLine = `${JSON.stringify(heartbeat)}\r\n`;

/**
 * Test server instance configured in setup
 */
let server;
/**
 * HelpClient instance configured in setup
 */
let client;

/**
 * Starts a server and creates client configured for server and assigns them to the client/server global vars.
 *
 *
 * @returns {Promise<{ client, server }>}
 */
async function setup(onConnection) {
  return new Promise((resolve, reject) => {
    server = net.createServer();

    if (onConnection) {
      server.on("connection", onConnection);
    }

    server.listen(0);
    doListen(server, err => {
      if (err) {
        reject(err);
      } else {
        const address = server.address();
        client = new HelpClient(name, address.host, address.port);
        resolve({ server, client });
      }
    });
  });
}

/**
 * Tears down server and client, if necessary
 *
 * @returns {Promise<void>}
 */
async function cleanup() {
  if (client) {
    const c = client;
    client = undefined;
    await c.close();
  }

  if (server) {
    const s = server;
    server = undefined;
    await s.close();
  }
}

describe("open()", () => {
  afterEach(cleanup);

  it("should fail if it cannot connect", async () => {
    const client = new HelpClient(name, "localhost", 0);
    await expect(client.open()).rejects.toThrow(/EADDRNOTAVAIL/);
  });

  it("should connect to server", async () => {
    const onConnection = jest.fn();
    await setup(onConnection);

    await client.open();
    await delay(10);
    expect(onConnection).toHaveBeenCalledTimes(1);
  });
}); // open()

describe("getTime()", () => {
  afterEach(cleanup);

  it("should return response matching request", async () => {
    const time1 = {
      date: "03:55/44",
      type: "msg",
      msg: { reply: null, time: "Sat Jun 29 03:55:33 2019", random: 20 },
      sender: "worker"
    };
    const time2 = {
      date: "03:55/44",
      type: "msg",
      msg: { reply: null, time: "Sat Jun 29 03:55:44 2019", random: 33 },
      sender: "worker"
    };

    await setup(socket => {
      readline.createInterface({ input: socket }).on("line", line => {
        try {
          const req = JSON.parse(line);
          if (req.name) {
            socket.write(welcomeLine);
          } else if (req.request === "time") {
            socket.write(`${JSON.stringify(time1)}\r\n`);
            time2.msg.reply = req.id;
            socket.write(`${JSON.stringify(time2)}\r\n`);
          }
        } catch (err) {
          console.error(line);
        }
      });
    });

    await client.open();
    await delay(10);
    const timeInfo = await client.getTime();
    const { time, random } = time2.msg;
    expect(timeInfo).toEqual({ time, random });
  });

  it("should fail if heartbeat reconnects", async () => {
    await setup();
    client.heartBeatInterval = 50;
    await client.open();
    await delay(10);
    const timeInfoPromise = client.getTime();

    await expect(timeInfoPromise).rejects.toThrow(/Socket disconnected/);
  });

  it("should fail if not connected", async () => {
    await setup();
    await expect(client.getTime()).rejects.toThrow(/Not connected to server/);
  });
}); // getTime()

describe("getCount()", () => {
  afterEach(cleanup);

  it("should return response matching request", async () => {
    const count1 = {
      sender: "worker",
      msg: { count: 10, reply: null },
      type: "msg",
      date: "03:55/38"
    };
    const count2 = {
      sender: "worker",
      msg: { count: 20, reply: null },
      type: "msg",
      date: "03:55/38"
    };

    await setup(socket => {
      readline.createInterface({ input: socket }).on("line", line => {
        try {
          const req = JSON.parse(line);
          if (req.name) {
            socket.write(welcomeLine);
          } else if (req.request === "count") {
            socket.write(`${JSON.stringify(count1)}\r\n`);
            count2.msg.reply = req.id;
            socket.write(`${JSON.stringify(count2)}\r\n`);
          }
        } catch (err) {
          console.error(line);
        }
      });
    });

    await client.open();
    await delay(10);
    const countInfo = await client.getCount();
    const { count } = count2.msg;
    expect(countInfo).toEqual({ count });
  });

  it("should fail if heartbeat reconnects", async () => {
    await setup();
    client.heartBeatInterval = 50;
    await client.open();
    await delay(10);
    const countInfoPromise = client.getCount();

    await expect(countInfoPromise).rejects.toThrow(/Socket disconnected/);
  });

  it("should fail if not connected", async () => {
    await setup();
    await expect(client.getCount()).rejects.toThrow(/Not connected to server/);
  });
}); // getCount()

describe("sendRequest(payload)", () => {
  afterEach(cleanup);

  it("should send serialized request", async () => {
    const payload = { foo: "bar" };
    const lineCallback = jest.fn();

    await setup(socket => {
      readline.createInterface({ input: socket }).on("line", lineCallback);
    });

    await client.open();
    await delay(10);
    await client.sendRequest(payload);
    await delay(10);
    expect(lineCallback).toHaveBeenCalledWith(JSON.stringify(payload));
  });
}); // sendRequest(payload)

describe("heartbeat", () => {
  let connectCount;
  let serverSocket;

  beforeEach(async () => {
    connectCount = 0;
    await setup(socket => {
      socket.write(welcomeLine);
      connectCount++;
      serverSocket = socket;
    });
  });

  afterEach(cleanup);

  it("should default to 2 seconds", async () => {
    expect(client.heartBeatInterval).toEqual(2000);
  });

  it("should stay connected while heart beat", async () => {
    // Use shorter interval for speedier tests
    client.heartBeatInterval = 50;
    await client.open();
    await delay(10);
    expect(connectCount).toEqual(1);
    serverSocket.write(heartbeatLine);
    await delay(40);
    serverSocket.write(heartbeatLine);
    await delay(40);
    expect(connectCount).toEqual(1);
  });

  it("should reconnect without heart beat", async () => {
    // Use shorter interval for speedier tests
    client.heartBeatInterval = 50;
    await client.open();
    await delay(10);
    expect(connectCount).toEqual(1);
    serverSocket.write(heartbeatLine);
    await delay(60);
    expect(connectCount).toEqual(2);
  });
}); // heartbeat

describe("events", () => {
  let connectCallback;
  let connectErrorCallback;
  let disconnectCallback;
  let responseErrorCallback;
  let responseCallback;

  async function setupTest(onConnection) {
    await setup(onConnection);
    client.on("connect", (connectCallback = jest.fn()));
    client.on("connect-error", (connectErrorCallback = jest.fn()));
    client.on("disconnect", (disconnectCallback = jest.fn()));
    client.on("response-error", (responseErrorCallback = jest.fn()));
    client.on("response", (responseCallback = jest.fn()));
  }

  afterEach(cleanup);

  function serverResponse(response) {
    server.on("connection", socket => {
      socket.write(welcomeLine);
      if (response) {
        socket.write(response);
      }
    });
  }

  it("should emit [connect] when socket connects", async () => {
    await setupTest();
    serverResponse("");

    await client.open();
    await delay(10);
    expect(connectErrorCallback).not.toHaveBeenCalled();
    expect(connectCallback).toHaveBeenCalledTimes(1);
  });

  it("should emit [connect-error] when socket fails to connect", async () => {
    await setupTest();
    await server.close();

    await client.open().catch(() => {});
    await delay(20);

    expect(connectErrorCallback).toHaveBeenCalledTimes(1);
    expect(connectCallback).not.toHaveBeenCalled();
  });

  it("should emit [response] for valid JSON response", async () => {
    await setupTest();
    serverResponse(`${JSON.stringify(response)}\r\n`);

    await client.open();
    await delay(10);
    expect(responseErrorCallback).not.toHaveBeenCalled();
    expect(responseCallback).toHaveBeenCalledTimes(2);
    expect(responseCallback).toHaveBeenCalledWith(response);
  });

  it("should emit [response-error] for invalid JSON response", async () => {
    await setupTest();
    const badJson = "This is not json";
    serverResponse(`${badJson}\r\n`);

    await client.open();
    await delay(10);
    expect(responseErrorCallback).toHaveBeenCalledWith(
      expect.objectContaining({
        line: badJson,
        error: expect.any(Error)
      })
    );
    expect(responseCallback).toHaveBeenCalledTimes(1);
  });

  it("should emit [disconnect] when client closes", async () => {
    await setupTest();
    serverResponse("");

    await client.open();
    await delay(10);
    await client.close();
    await delay(10);
    expect(connectCallback).toHaveBeenCalledTimes(1);
    expect(responseErrorCallback).not.toHaveBeenCalled();
    expect(disconnectCallback).toHaveBeenCalledTimes(1);
  });

  it("should emit [disconnect] when server disconnects", async () => {
    await setupTest(async socket => {
      await delay(10);
      socket.destroy();
    });
    serverResponse("");

    await client.open();
    await delay(20);

    expect(connectCallback).toHaveBeenCalledTimes(1);
    expect(responseErrorCallback).not.toHaveBeenCalled();
    expect(disconnectCallback).toHaveBeenCalledTimes(1);
  });
}); // events
