const log4js = require("log4js");
const log = log4js.getLogger("app");
const readline = require("readline-promise").default;
const { HelpClient, EVENT_RESPONSE_ERROR } = require("./HelpClient");

const SHOW_RESPONSE_ERRORS = false;

const loglevel = process.env.LOG_LEVEL || "error";

log4js.configure({
  appenders: {
    file: { type: "file", filename: "help-client-app.log" }
  },
  categories: {
    default: { appenders: ["file"], level: loglevel }
  }
});

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

(async () => {
  log.info("application started");

  const name = await rl.questionAsync("Enter you user name: ");
  log.info("connecting user:", name);

  const client = new HelpClient(name, "35.226.214.55", 9432);

  if (SHOW_RESPONSE_ERRORS) {
    // Log malformed response info to console
    client.on(EVENT_RESPONSE_ERROR, res =>
      console.error("Response error:", res.line)
    );
  }

  await client.open();

  console.log(
    "Enter a command (time, count) to execute; connect to reconnect; a JSON object to send to server; exit to quit."
  );

  let command;
  while ((command = await rl.questionAsync("> ")) !== "exit") {
    try {
      await processCommand(client, command);
    } catch (err) {
      console.error("Request failed:", err.message);
    }
  }
  await client.close();
  log.info("application completed");
  console.log("Goodbye!");
})()
  .catch(err => {
    log.error("application failed:", err);
    console.error("Application failed:", err.message);
  })
  .finally(() => {
    rl.close();
  });

/**
 * Process a line of user input
 *
 * @param {HelpClient} client
 * @param {string} command input from user
 * @returns {Promise<void>}
 */
async function processCommand(client, command) {
  switch (command) {
    case "":
      // Ignore empty lines
      break;
    case "connect":
      try {
        await client.open();
        console.log("Connected");
      } catch (err) {
        console.log("Failed to connect:", err.message);
      }
      break;
    case "time":
      {
        const { time, random } = await client.getTime();
        console.log("Time:", time);
        if (random > 30) {
          console.warn("Random is bigger than 30");
        }
      }
      break;
    case "count":
      {
        const { count } = await client.getCount();
        console.log("Count:", count);
      }
      break;
    default: {
      let payload;

      try {
        payload = JSON.parse(command);
        // Ensure we have a valid object
        if (!payload || payload.constructor !== Object) {
          throw new Error("Not an object");
        }
      } catch (err) {
        console.error("Invalid payload:", err.message);
        log.warn("invalid user input:", err.message, `[${command}]`);
        payload = undefined;
      }

      if (payload) {
        await client.sendRequest(payload);
      }
    }
  }
}
