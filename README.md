# HelpClient

Sample CLI to talk to a TCP server.

### Installation

The app requires Node 10 (it uses `Promise.finally`). 

```
nvm use
npm install
```

### Usage

```
# via npm
npm start

# with custom log level
LOG_LEVEL=debug node lib/index.js
```

Simple CLI that prompts for user name and then accepts line-wise input:

* `exit` - stops application
* `connect` - reconnect with server (useful if connection dropped by server)
* `count` - queries server for number of commands and prints to console
* `time` - queries server for time and prints time to console.
  * Also prints random value in respsone if it is > 30
* _JSON object_ - sends object to server (e.g. `{"foo":"bar"}`) 

Log messages go to `help-client-app.log`.

### Test

```
npm test
```

I usually use mocha/sinon/chai for testing, but I wanted to try out `Jest` for backend testing.
It's very developer-friendly when used in watch mode (`npm run test:watch`).

### Notes

* I used `telnet 35.226.214.55 9432` to get a better understanding of the data sent by the server.
(The server seems tends to go down quite a bit)
* The CLI is in `lib/index.js`.  It would need to be refactored to be testable.
* The core functionality is in `HelpClient.js` and most public methods are covered by tests.
* The assignment didn't mention format of EOLN.  The app sends `\r\n` and accepts either `\n` or `\r\n`.

### HelpClient

* `constructor(name, host, port)` - instantiates client
* `connect()` - connects to server, starts monitoring heartbeat and sends login request
* `sendRequest(payload)` - serializes payload to JSON and sends it to server on a single line
* Request commands generate an id to be able to correlate response with request
  * `getTime()` - sends **time** request and returns `{ time, random }`
  * `getCount()` - sends **count** request and returns `{ count }`
* `close()` - stops monitoring heartbeat and closes connection to server
* The HelpClient emits the following events (some it uses internally):
  * `"connect" (socket)`- when it (re)establishes connection with server
  * `"connect-error" (error)`- when connectin to server fails
  * `"response (payload)"`- when it receives a valid JSON line from server (`payload` is deserialized)
  * `"response-error ({ line, error })` - when it receives invalid JSON from server
  * `"disconnect"`- when connection with server is closed
* Async functions return promises (rather than using callbacks)

---

Author: Stepan Riha

License: MIT
